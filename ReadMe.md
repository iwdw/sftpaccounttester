# SFTP Account Tester 

This solution consists of two projects.  The first project is SftpTest which contains the class SftpConnect.  This class is everything needed to connect via sftp to a server and list the contents of a supplied Data Directory.  The second project is sftpverify which is a command line tool to gather the information to test the sftp account and uses SftpTest perform the actual test.

## Using SftpConnect Object

1. Import the SftpConnect Class library�s project.  VB Example: Imports SftpTest
2. When you substantiate the SftpConnect object its constructor takes 4 arguments.  The sftp account (1) User Name, (2) Password, (3) comma separated data Directories, and an optional (4) hostname.  If a hostname is not supplied, it will use valkyr.cira.colostate.edu.
3. Once the SftpTest object has been instantiated if you call the function RunTest() it will make a connection via sftp to the server and list the contents of any data directory provided.  It will return these results as a List of strings for display to the user or further processing.

The sftpverify project is a C# example of using SftpTest. 

### Project External Dependences
SSH.NET (2016.1.0) NuGet package.  This is the Renci.SshNet.dll.

