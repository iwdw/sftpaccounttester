Imports sftpnet = Renci.SshNet

''' <summary>
''' This class is everything needed to connect via sftp to a server and list the contents of a supplied Data Directory.
''' </summary>
Public Class SftpConnect
    Protected _UserName As String
    Protected _Password As String
    Protected _DataDir As List(Of String)
    Protected _Server As String

    ''' <summary>
    ''' The user name of the sftp account as a string.
    ''' </summary>
    ''' <returns>The User name</returns>
    Public Property UserName As String
        Get
            Return _UserName
        End Get
        Set(value As String)
            _UserName = value
        End Set
    End Property

    ''' <summary>
    ''' Write only password for the associated User Account.
    ''' </summary>
    Public WriteOnly Property Password As String
        Set(value As String)
            _Password = value
        End Set
    End Property

    ''' <summary>
    ''' I comma seperated string list of user Data Directories is expected. 
    ''' </summary>
    ''' <returns>
    ''' The comma seperated list of files.
    ''' </returns>
    Public Property DataDir As String
        Get
            Return _DataDir.ToString()
        End Get
        Set(value As String)
            'Its getting stored as a List data type.
            _DataDir = ProcessDirs(value)
        End Set
    End Property

    ''' <summary>
    ''' This is the URL of the Sftp Server you are connecting too.
    ''' The defualt is valkyr.cira.colostate.edu.
    ''' </summary>
    ''' <returns>The Sftp Server's name</returns>
    Public Property SftpServer As String
        Get
            Return _Server
        End Get
        Set(value As String)
            _Server = value
        End Set
    End Property

    ''' <summary>
    ''' Constructor
    ''' </summary>
    Public Sub New()
        'Nothing to do
    End Sub

    ''' <summary>
    ''' A Nice constructor to fill in all properties at once.
    ''' </summary>
    ''' <param name="UserName"></param>
    ''' <param name="Password"></param>
    ''' <param name="DataDir"></param>
    ''' <param name="SftpServer"></param>
    Public Sub New(ByVal UserName As String, ByVal Password As String, ByVal DataDir As String, Optional ByVal SftpServer As String = "valkyr.cira.colostate.edu")
        _UserName = UserName
        _Password = Password
        _DataDir = ProcessDirs(DataDir)
        _Server = SftpServer
    End Sub

    ''' <summary>
    ''' Runs a simple connection test and checks Data Directories.
    ''' Assuming the properties UserName, Password, DataDir, and SftpServer properties have all been set correctly.
    ''' </summary>
    Public Function RunTest() As List(Of String)
        Dim ResultList = New List(Of String)
        Dim connInfo = New sftpnet.ConnectionInfo(_Server, _UserName,
                                                  New sftpnet.PasswordAuthenticationMethod(_UserName, _Password),
                                                  New sftpnet.PrivateKeyAuthenticationMethod("rsa.key"))
        Using client = New sftpnet.SftpClient(connInfo)
            Try
                client.Connect()
                'First get everything in the home directory
                'Dim data = client.ListDirectory("")
                'ResultList.Add("In FTP User Home:")
                'For Each item In data
                '    ResultList.Add(item.Name)
                'Next
                'Now for each data directory
                'ResultList.Add("In First Level of Data Directories:")
                For Each item In _DataDir
                    Dim results = client.ListDirectory(item)
                    For Each piece In results
                        ResultList.Add(piece.FullName)
                    Next

                Next

            Catch x As Exception
                ResultList.Add("Error: " + x.Message)
            Finally
                client.Disconnect()
            End Try
            Return ResultList
        End Using

    End Function

    ''' <summary>
    ''' Break down string of Directories into a List type of them.
    ''' </summary>
    ''' <param name="Data">A comma seperated list of Data Directories in a string.</param>
    ''' <returns></returns>
    Private Function ProcessDirs(ByVal Data As String) As List(Of String)
        Return Data.Split(",").ToList()
    End Function

End Class

