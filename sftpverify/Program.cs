﻿using System;
using SftpTest;

namespace sftpverify
{
    class Program
    {
        /// <summary>
        /// Simple test to gather information and to test the sftp account.
        /// </summary>
        /// <param name="args">Ignored. None expected.</param>
        static void Main(string[] args)
        {
            Boolean continueOn = true;
            while (continueOn)
            {
                //Gather Data
                Console.Write("Enter User Name: ");
                var userName = Console.ReadLine();
                Console.Write("Enter Password: ");
                var password = Console.ReadLine();
                Console.Write("Enter Data Directories: ");
                var dataDir = Console.ReadLine();
                Console.WriteLine("Connecting to host....");

                //Connect
                var SftpTest = new SftpTest.SftpConnect(userName, password, dataDir);
                var results = SftpTest.RunTest();
                foreach (var item in results)
                {
                    Console.WriteLine(item);
                }
                
                //Another?
                Console.Write("\nEnter \'y\' to enter another or something else to quit: ");
                ConsoleKeyInfo yn = Console.ReadKey();
                if (yn.Key != ConsoleKey.Y) { continueOn = false; /*Goodbye*/ } else { Console.WriteLine(""); }
            }
        }
    }
}
